import sqlite3
from flask import Flask, render_template, request, redirect, url_for, session, jsonify, flash, send_file, make_response
import bcrypt
from werkzeug.utils import secure_filename, send_file
import os
from fpdf import FPDF

app = Flask(__name__)
app.secret_key = 'admin'

# Configurar la carpeta donde se guardarán las imágenes
app.config['UPLOAD_FOLDER'] = 'static/imagenes_productos'

@app.route('/realizar_compra')
def realizar_compra():
    # Obtener el carrito de la sesión
    carrito = obtener_carrito()
    total_carrito = calcular_total_carrito(carrito)

    # Generar el PDF de la factura
    factura_path = generar_factura_pdf(carrito, total_carrito)

    # Borrar el carrito de la sesión
    session.pop('carrito', None)

    # Leer el contenido del PDF
    with open(factura_path, 'rb') as file:
        pdf_data = file.read()

    # Crear una respuesta con los datos del PDF
    response = make_response(pdf_data)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = f'attachment; filename=factura.pdf'

    return response


def generar_factura_pdf(carrito, total_carrito):
    # Crear el PDF
    pdf = FPDF()
    pdf.add_page()

    # Agregar contenido al PDF
    pdf.set_font("Arial", size=12)
    pdf.cell(200, 10, txt="Factura de Compra", ln=True, align="C")
    pdf.cell(200, 10, ln=True)
    for item in carrito:
        pdf.cell(0, 10, txt=f"{item['nombre']} - Cantidad: {item['cantidad']} - Precio: ${item['precio'] * item['cantidad']}", ln=True)

    pdf.cell(200, 10, ln=True)
    pdf.cell(0, 10, txt=f"Total: ${total_carrito}", ln=True)

    # Guardar el PDF en el servidor
    factura_path = os.path.join(os.getcwd(), "factura.pdf")
    pdf.output(factura_path)

    return factura_path

@app.route('/carrito')
def carrito():
    # Verificar si el usuario está logueado (que exista la clave 'email' en la sesión)
    if 'email' not in session:
        # Si no está logueado, redirigir al login
        return redirect(url_for('login'))

    # Verificar si hay productos en el carrito de la sesión
    if 'carrito' not in session or not session['carrito']:
        # Si no hay productos en el carrito, mostrar mensaje de carrito vacío
        return render_template('carrito.html', carrito=[], total_carrito=0, factura_path=None)

    # Obtener el carrito de la sesión
    carrito = session['carrito']
    total_carrito = calcular_total_carrito(carrito)

    return render_template('carrito.html', carrito=carrito, total_carrito=total_carrito, factura_path=None)


@app.route('/descargar_factura')
def descargar_factura():
    factura_path = session.get('factura_path')
    if factura_path:
        session.pop('factura_path', None)
        return send_file(factura_path, as_attachment=True, attachment_filename='factura.pdf')
    else:
        return redirect(url_for('comprar'))

# Función para calcular el total del carrito de compras
def calcular_total_carrito(carrito):
    total = 0
    for item in carrito:
        total += item['precio'] * item['cantidad']
    return total

@app.route('/registro', methods=['GET', 'POST'])
def registro():
    if request.method == 'POST':
        # Obtener los datos del formulario de registro
        nombre = request.form['nombre']
        email = request.form['email']
        password = request.form['password']

        # Verificar si el usuario ya está registrado
        conn = sqlite3.connect('productos.db')
        cursor = conn.cursor()
        cursor.execute('SELECT * FROM usuarios WHERE email=?', (email,))
        usuario_existente = cursor.fetchone()
        conn.close()

        if usuario_existente:
            error = 'Ya existe un usuario registrado con este correo electrónico.'
            return render_template('registro.html', error=error)

        # Hash de la contraseña antes de guardarla en la base de datos
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

        # Conectar a la base de datos y agregar el nuevo usuario
        conn = sqlite3.connect('productos.db')
        cursor = conn.cursor()
        cursor.execute('INSERT INTO usuarios (nombre, email, password, es_admin) VALUES (?, ?, ?, ?)',
                       (nombre, email, hashed_password, 0))  # 0 para indicar que es un usuario normal, no admin
        conn.commit()
        conn.close()

        # Mostrar mensaje de éxito
        mensaje_exito = 'Registro exitoso. Ahora puedes iniciar sesión.'
        return render_template('registro.html', mensaje_exito=mensaje_exito)
    return render_template('registro.html')

# Ruta para cerrar sesión (logout)
@app.route('/logout')
def logout():
    # Eliminar la información de la sesión (cerrar sesión)
    session.pop('email', None)
    return redirect(url_for('login'))


@app.route('/celulares')
def celulares():
    # Conectar a la base de datos
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Obtener los productos con la categoría "Celulares" de la base de datos
    cursor.execute('SELECT * FROM productos WHERE categoria = ?', ('Celulares',))
    productos_celulares = cursor.fetchall()

    # Cerrar la conexión a la base de datos
    conn.close()

    # Renderizar la plantilla comprar.html y pasar la lista de productos de la categoría "Celulares"
    return render_template('comprar.html', productos=productos_celulares)


# Ruta para la página de computadoras
@app.route('/computadoras')
def computadoras():
    # Aquí puedes obtener los productos de la base de datos que pertenecen a la categoría "Computadoras"
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM productos WHERE categoria=?', ('Computadoras',))
    productos = cursor.fetchall()
    conn.close()

    return render_template('comprar.html', productos=productos)

def obtener_producto_por_id(producto_id):
    # Conectar a la base de datos
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Obtener el producto por su ID
    cursor.execute('SELECT id, nombre, descripcion, precio, categoria, imagen FROM productos WHERE id = ?', (producto_id,))
    producto = cursor.fetchone()

    # Cerrar la conexión a la base de datos
    conn.close()

    # Devolver el producto encontrado
    return producto

# Ruta para obtener el contenido del carrito de compras
@app.route('/obtener_carrito', methods=['GET'])
def obtener_carrito():
    # Verificar si el carrito de compras existe en la sesión
    if 'carrito' not in session:
        return []

    # Obtener los datos del carrito de compras de la sesión
    carrito_productos = session['carrito']

    return carrito_productos



# Ruta para agregar un producto al carrito de compras
@app.route('/agregar_al_carrito/<int:producto_id>/<int:cantidad>', methods=['POST'])
def agregar_al_carrito(producto_id, cantidad):
    # Conectar a la base de datos
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Obtener el producto por su ID
    cursor.execute('SELECT * FROM productos WHERE id = ?', (producto_id,))
    producto = cursor.fetchone()

    # Verificar si el producto existe
    if producto is None:
        return jsonify({'success': False, 'message': 'Producto no encontrado'})

    # Verificar si el carrito de compras ya existe en la sesión
    if 'carrito' not in session:
        session['carrito'] = []

    # Verificar si el producto ya está en el carrito
    for item in session['carrito']:
        if item['id'] == producto_id:
            item['cantidad'] += cantidad
            break
    else:
        # Si el producto no está en el carrito, agregarlo
        item_carrito = {
            'id': producto_id,
            'nombre': producto[1],
            'descripcion': producto[2],
            'precio': producto[3],
            'categoria': producto[4],
            'imagen': producto[5],
            'cantidad': cantidad
        }
        session['carrito'].append(item_carrito)

    # Guardar los cambios en la sesión
    session.modified = True

    # Cerrar la conexión a la base de datos
    conn.close()

    return jsonify({'success': True, 'message': 'Producto añadido al carrito'})


# Ruta para la página de consolas
@app.route('/consolas')
def consolas():
    # Aquí puedes obtener los productos de la base de datos que pertenecen a la categoría "Consolas"
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM productos WHERE categoria=?', ('Consolas',))
    productos = cursor.fetchall()
    conn.close()

    return render_template('comprar.html', productos=productos)


# Ruta para la página de accesorios
@app.route('/accesorios')
def accesorios():
    # Aquí puedes obtener los productos de la base de datos que pertenecen a la categoría "Accesorios"
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM productos WHERE categoria=?', ('Accesorios',))
    productos = cursor.fetchall()
    conn.close()

    return render_template('comprar.html', productos=productos)


# Función para verificar las credenciales de inicio de sesión
def verificar_credenciales(email, password):
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Buscar al usuario en la base de datos por su dirección de correo electrónico
    cursor.execute('SELECT password FROM usuarios WHERE email=?', (email,))
    resultado = cursor.fetchone()

    conn.close()

    if resultado:
        hashed_password = resultado[0]
        if bcrypt.checkpw(password.encode('utf-8'), hashed_password):
            # Si la contraseña coincide, el inicio de sesión es válido
            return True
    return False


# Función para determinar si el usuario es administrador
def es_administrador(email):
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Buscar al usuario en la base de datos por su dirección de correo electrónico
    cursor.execute('SELECT es_admin FROM usuarios WHERE email=?', (email,))
    resultado = cursor.fetchone()

    conn.close()

    if resultado and resultado[0] == 1:
        # Si el resultado es válido y el campo "es_admin" es igual a 1, el usuario es un administrador
        return True
    return False


@app.route('/agregar_producto', methods=['GET', 'POST'])
def agregar_producto():
    if request.method == 'POST':
        # Obtener los datos del formulario
        nombre = request.form['nombre']
        descripcion = request.form['descripcion']
        precio = float(request.form['precio'])
        categoria = request.form['categoria']

        # Obtener el archivo de imagen enviado por el formulario
        imagen = request.files['imagen']

        # Verificar si se envió una imagen válida
        if imagen and imagen.filename != '':
            # Obtener el nombre seguro del archivo para evitar problemas con el nombre del archivo en el sistema de archivos
            nombre_archivo = secure_filename(imagen.filename)
            # Guardar el archivo de imagen en la carpeta de imágenes
            imagen.save(os.path.join(app.config['UPLOAD_FOLDER'], nombre_archivo))
        else:
            # Si no se envió una imagen, puedes manejar esto según tus requerimientos
            # Por ejemplo, podrías mostrar un mensaje de error al usuario
            pass

        # Conectar a la base de datos
        conn = sqlite3.connect('productos.db')
        cursor = conn.cursor()

        # Insertar el nuevo producto en la tabla "productos"
        cursor.execute('INSERT INTO productos (nombre, descripcion, precio, categoria, imagen) VALUES (?, ?, ?, ?, ?)',
                       (nombre, descripcion, precio, categoria, nombre_archivo))

        # Guardar los cambios y cerrar la conexión
        conn.commit()
        conn.close()

        # Responder con un JSON para indicar el resultado exitoso
        return jsonify({'result': 'success'})

        # Si el método es GET, renderizar el formulario para agregar un nuevo producto
    return render_template('formulario_agregar_producto.html')


# Ruta para la página de modificación de producto
@app.route('/modificar_producto/<int:id>', methods=['GET', 'POST'])
def modificar_producto(id):
    if request.method == 'POST':
        # Obtener los datos del formulario de modificación
        nombre = request.form['nombre']
        descripcion = request.form['descripcion']
        precio = float(request.form['precio'])
        categoria = request.form['categoria']

        # Conectar a la base de datos
        conn = sqlite3.connect('productos.db')
        cursor = conn.cursor()

        # Actualizar el producto en la tabla "productos"
        cursor.execute('UPDATE productos SET nombre=?, descripcion=?, precio=?, categoria=? WHERE id=?',
                       (nombre, descripcion, precio, categoria, id))

        # Guardar los cambios y cerrar la conexión
        conn.commit()
        conn.close()

        # Mostrar mensaje flash de que se modificó correctamente
        flash('El producto se modificó correctamente', 'success')

        # Redirigir al mismo formulario de modificación del producto
        return redirect(url_for('modificar_producto', id=id))

    # Si el método es GET, renderizar el formulario de modificación del producto
    # Obtener el producto de la base de datos para mostrar los datos actuales en el formulario
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM productos WHERE id=?', (id,))
    producto = cursor.fetchone()
    conn.close()

    return render_template('formulario_modificar_producto.html', producto=producto)


# Ruta para eliminar un producto (ejemplo)
@app.route('/eliminar_producto/<int:idProducto>', methods=['POST'])
def eliminar_producto(idProducto):
    # Conectar a la base de datos
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Eliminar el producto de la tabla "productos"
    cursor.execute('DELETE FROM productos WHERE id=?', (idProducto,))

    # Guardar los cambios y cerrar la conexión
    conn.commit()
    conn.close()

    # Responder con un JSON para indicar el resultado exitoso
    return jsonify({'result': 'success'})


# Ruta para la página de inicio
@app.route('/')
def index():
    # Verificar si el usuario está autenticado
    if 'email' not in session:
        return redirect(url_for('login'))

    # Si el usuario está autenticado, redirigir a la página de compras o administración según su rol
    email = session['email']
    if es_administrador(email):
        return redirect(url_for('admin'))
    else:
        return redirect(url_for('comprar'))

    # Aquí puedes obtener los productos de la base de datos y mostrarlos en la página de inicio
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM productos')
    productos = cursor.fetchall()
    conn.close()

    return render_template('index.html', productos=productos)


# Ruta para la página de compras
@app.route('/comprar')
def comprar():
    # Aquí puedes obtener los productos de la base de datos y mostrarlos en la página de compras
    # Obtener los productos de la base de datos
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()
    cursor.execute('SELECT * FROM productos')
    productos = cursor.fetchall()
    conn.close()

    return render_template('comprar.html', productos=productos)


# Ruta para la página de administración (solo accesible para administradores)
@app.route('/admin')
def admin():
    # Aquí puedes implementar la lógica para mostrar la página de administración
    if 'email' in session and es_administrador(session['email']):
        # Obtener los productos de la base de datos
        conn = sqlite3.connect('productos.db')
        cursor = conn.cursor()
        cursor.execute('SELECT id, nombre, descripcion, precio, categoria, imagen FROM productos')
        productos = cursor.fetchall()
        conn.close()

        # Agregar la URL de la imagen y la categoría a la tupla producto
        productos_con_imagen_y_categoria = []
        for producto in productos:
            producto_id, nombre, descripcion, precio, categoria, imagen = producto
            imagen_url = url_for('static', filename='imagenes_productos/' + imagen)
            producto_con_imagen_y_categoria = (producto_id, nombre, descripcion, precio, categoria, imagen_url)
            productos_con_imagen_y_categoria.append(producto_con_imagen_y_categoria)

        return render_template('admin.html', productos=productos_con_imagen_y_categoria)
    else:
        return redirect(url_for('login'))


# Ruta para la página de inicio de sesión
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        # Verificar las credenciales ingresadas
        if verificar_credenciales(email, password):
            # Credenciales válidas, iniciar sesión y redirigir a la página correspondiente
            session['email'] = email
            session['carrito'] = []

            # Si el usuario es administrador, redirigir a la página "admin", de lo contrario, redirigir a "comprar"
            if es_administrador(email):
                return redirect(url_for('admin'))
            else:
                return redirect(url_for('comprar'))

        else:
            # Credenciales inválidas, mostrar mensaje de error
            error = "Correo electrónico o contraseña incorrectos. Por favor, intenta nuevamente."
            return render_template('login.html', error=error)

    return render_template('login.html')

@app.route('/eliminar_del_carrito/<int:producto_id>', methods=['POST'])
def eliminar_producto_del_carrito(producto_id):
    # Verificar que el usuario esté logueado (que exista la clave 'email' en la sesión)
    if 'email' not in session:
        # Si no está logueado, redirigir al login
        return redirect(url_for('login'))

    # Verificar que el producto esté en el carrito del usuario
    if 'carrito' not in session:
        # Si no hay carrito, redirigir a la página de comprar
        return redirect(url_for('comprar'))

    # Obtener el carrito de compras del usuario de la sesión
    carrito = session['carrito']

    # Buscar el producto en el carrito por su ID
    for producto in carrito:
        if producto['id'] == producto_id:
            # Si se encontró el producto en el carrito, eliminarlo
            carrito.remove(producto)

            # Guardar el carrito actualizado en la sesión
            session['carrito'] = carrito

            # Devolver una respuesta exitosa en formato JSON
            return jsonify({"success": True})

    # Si el producto no se encontró en el carrito, devolver una respuesta de error en formato JSON
    return jsonify({"success": False, "message": "El producto no se encontró en el carrito"})



if __name__ == '__main__':
    app.run(debug=True)
