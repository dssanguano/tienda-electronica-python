import sqlite3
import bcrypt

# Función para insertar un usuario en la tabla "usuarios"
def insertar_usuario(nombre, email, password, es_admin):
    conn = sqlite3.connect('productos.db')
    cursor = conn.cursor()

    # Verificar si el usuario ya existe en la base de datos por su dirección de correo electrónico
    cursor.execute('SELECT id FROM usuarios WHERE email=?', (email,))
    existe_usuario = cursor.fetchone()

    if not existe_usuario:
        # Hashear la contraseña antes de almacenarla en la base de datos
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

        # Insertar el nuevo usuario en la tabla "usuarios"
        cursor.execute('INSERT INTO usuarios (nombre, email, password, es_admin) VALUES (?, ?, ?, ?)',
                       (nombre, email, hashed_password, es_admin))
        conn.commit()
        print(f"Usuario '{nombre}' ha sido creado con éxito.")
    else:
        print(f"Error: El usuario con el correo electrónico '{email}' ya existe.")

    conn.close()

# Ejemplo de cómo insertar un usuario administrador
insertar_usuario('David Salazar', 'admin@gmail.com', 'admin', 1)

# Ejemplo de cómo insertar un usuario normal
insertar_usuario('Juan Perez', 'jperez@gmail.com', 'jperez', 0)
