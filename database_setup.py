import sqlite3

# Conexión a la base de datos (creará el archivo si no existe)
conn = sqlite3.connect('productos.db')

# Crear un cursor para interactuar con la base de datos
cursor = conn.cursor()

# Crear la tabla de productos si no existe
cursor.execute('''
    CREATE TABLE IF NOT EXISTS productos (
        id INTEGER PRIMARY KEY,
        nombre TEXT NOT NULL,
        descripcion TEXT,
        precio REAL NOT NULL,
        imagen TEXT,
        categoria TEXT NOT NULL
    )
''')

# Crear la tabla de usuarios si no existe
cursor.execute('''
    CREATE TABLE IF NOT EXISTS usuarios (
        id INTEGER PRIMARY KEY,
        nombre TEXT NOT NULL,
        email TEXT NOT NULL,
        password TEXT NOT NULL,
        es_admin INTEGER NOT NULL
    )
''')

# Guardar los cambios y cerrar la conexión
conn.commit()
conn.close()
