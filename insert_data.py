import sqlite3

# Conexión a la base de datos
conn = sqlite3.connect('productos.db')
cursor = conn.cursor()

# Datos de ejemplo (puedes agregar más productos si lo deseas)
productos_ejemplo = [
    (1, 'Producto 1', 'Descripción del Producto 1', 500, 'imagen1.jpg', 'Celulares'),
    (2, 'Producto 2', 'Descripción del Producto 2', 1000, 'imagen2.jpg', 'Computadoras'),
    (3, 'Producto 3', 'Descripción del Producto 3', 300, 'imagen3.jpg', 'Consolas'),
    # Agrega más productos aquí...
]

# Insertar datos en la tabla productos
cursor.executemany('INSERT INTO productos VALUES (?, ?, ?, ?, ?, ?)', productos_ejemplo)

# Guardar los cambios y cerrar la conexión
conn.commit()
conn.close()

