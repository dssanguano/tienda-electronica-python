// script.js

// Función para mostrar la alerta de éxito al agregar un producto
function mostrarAlertaExito() {
    Swal.fire({
        title: 'Producto agregado',
        text: 'El producto se agregó correctamente.',
        icon: 'success',
        timer: 2000, // Mostrará la alerta por 2 segundos
        timerProgressBar: true,
        showConfirmButton: false
    });
}

// Función para agregar un producto
function agregarProducto() {
    // Obtener los valores del formulario
    var nombre = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;
    var precio = document.getElementById('precio').value;
    var categoria = document.getElementById('categoria').value;
    var imagen = document.getElementById('imagen').files[0]; // Obtiene el archivo de imagen

    // Crear un objeto FormData para enviar los datos del formulario
    var formData = new FormData();
    formData.append('nombre', nombre);
    formData.append('descripcion', descripcion);
    formData.append('precio', precio);
    formData.append('categoria', categoria);
    formData.append('imagen', imagen);

    // Realizar una petición AJAX para agregar el producto
    fetch("/agregar_producto", {
        method: "POST",
        body: formData
    })
    .then((response) => response.json())
    .then((data) => {
        // Si el producto se agregó correctamente, mostrar la alerta de éxito
        if (data.result === "success") {
            mostrarAlertaExito(); // Mostrar la alerta de éxito
            // Limpiar el formulario si es necesario
            document.getElementById('nombre').value = '';
            document.getElementById('descripcion').value = '';
            document.getElementById('precio').value = '';
            document.getElementById('categoria').value = '';
            document.getElementById('imagen').value = '';
        } else {
            console.error("Error al agregar el producto.");
        }
    })
    .catch((error) => {
        console.error("Error en la petición AJAX:", error);
    });
}
