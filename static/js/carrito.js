// carrito.js

function agregarAlCarrito(producto) {
    // Obtener el carrito de compras del almacenamiento local (si existe)
    var carrito = JSON.parse(localStorage.getItem('carrito')) || [];

    // Verificar si el producto ya está en el carrito
    var productoEnCarrito = carrito.find(item => item.id === producto.id);
    if (!productoEnCarrito) {
        // Si el producto no está en el carrito, agregarlo
        carrito.push(producto);

        // Guardar el carrito actualizado en el almacenamiento local
        localStorage.setItem('carrito', JSON.stringify(carrito));

        // Mostrar un mensaje de confirmación
        alert("Producto agregado al carrito");
    } else {
        // Si el producto ya está en el carrito, mostrar un mensaje de error
        alert("El producto ya está en el carrito");
    }
}

function mostrarMensajeCompraRealizada() {
    Swal.fire({
        icon: 'success',
        title: '¡Compra realizada!',
        text: 'Gracias por tu compra. Recibirás los productos en breve.',
        timer: 5000,  // Puedes ajustar el tiempo según tus preferencias
        showConfirmButton: false
    });
}