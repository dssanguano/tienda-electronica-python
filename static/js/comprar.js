// Script para la funcionalidad del botón Añadir al carrito
document.querySelectorAll('.btn-add-cart').forEach(button => {
    button.addEventListener('click', () => {
        const productId = button.dataset.productId;
        const cantidad = document.getElementById(`cantidad-${productId}`).value;

        // Hacer la petición AJAX para agregar el producto al carrito
        fetch(`/agregar_al_carrito/${productId}/${cantidad}`, { method: 'POST' })
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Producto añadido al carrito',
                        text: 'El producto ha sido añadido al carrito de compras.',
                        timer: 3000,
                        showConfirmButton: false
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Hubo un error al añadir el producto al carrito. Inténtalo de nuevo.',
                    });
                }
            })
            .catch(error => {
                console.error('Error en la petición AJAX:', error);
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Hubo un error al añadir el producto al carrito. Inténtalo de nuevo.',
                });
            });
    });
});

// Obtener el carrito de compras y mostrarlo en una página de carrito
document.getElementById('btn-carrito').addEventListener('click', () => {
    window.location.href = '/carrito';
});

// Función para cerrar la pantalla flotante del carrito
function cerrarCarrito() {
    Swal.close();
}

