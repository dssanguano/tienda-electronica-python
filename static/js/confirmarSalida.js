// Función para mostrar la alerta de SweetAlert2 al hacer clic en el botón "Salir"
function mostrarAlertaSalir() {
    Swal.fire({
        title: '¿Estás seguro?',
        text: 'Se cerrará la sesión y volverás al inicio de sesión.',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, salir',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            // Si el usuario hace clic en "Sí, salir", cerrar la sesión y redirigir al inicio de sesión
            fetch("/logout", {
                method: "GET",
            })
            .then((response) => {
                if (response.ok) {
                    window.location.href = response.url;
                } else {
                    console.error("Error al cerrar sesión.");
                }
            })
            .catch((error) => {
                console.error("Error en la petición AJAX:", error);
            });
        }
    });
}

function confirmarSalida() {
    mostrarAlertaSalir(); // Llama a la función personalizada para mostrar la alerta
}