// Función para eliminar un producto
function eliminarProducto(idProducto) {
    Swal.fire({
        title: '¿Estás seguro?',
        text: 'Esta acción eliminará el producto de forma permanente.',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Sí, eliminar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            // Realizar una petición AJAX para eliminar el producto
            fetch("/eliminar_producto/" + idProducto, {
                method: "POST"
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.result === "success") {
                    // Si el producto se eliminó correctamente, recargar la página para actualizar la tabla de productos
                    location.reload();
                } else {
                    console.error("Error al eliminar el producto.");
                }
            })
            .catch((error) => {
                console.error("Error en la petición AJAX:", error);
            });
        }
    });
}