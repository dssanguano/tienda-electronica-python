function modificarProducto(id) {
    // Obtener los datos del formulario
    var formData = new FormData(document.getElementById('formularioModificarProducto'));

    // Hacer la petición AJAX
    $.ajax({
        type: 'POST',
        url: '/modificar_producto/' + id,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            // Mostrar el mensaje de éxito con SweetAlert2
            Swal.fire({
                icon: 'success',
                title: 'Producto Modificado',
                text: 'El producto se modificó correctamente.',
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                // Redirigir al mismo formulario de modificación del producto
                window.location.href = '/modificar_producto/' + id;
            });
        },
        error: function(xhr, status, error) {
            // Mostrar el mensaje de error con SweetAlert2
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Hubo un error al modificar el producto.',
                confirmButtonText: 'Aceptar'
            });
        }
    });
}
